﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace ControlSystem
{
    [Serializable]
    public class Course : BaseSerializer, ICourse, INotifyPropertyChanged
    {
        private string _name;
        public Course()
        {
            Themes = new ObservableCollection<ITheme>();
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value!=_name)
                {
                    _name = value;
                    OnPropertyChanged();
                }
            }
        }
        public ICollection<ITheme> Themes { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
