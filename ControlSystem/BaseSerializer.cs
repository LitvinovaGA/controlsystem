﻿using ControlSystem.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ControlSystem
{
    public class BaseSerializer : ISerializable
    {
        public object Deserialize(string filename)
        {
            var formatter = new BinaryFormatter();
            using (var filesStream = new FileStream(filename, FileMode.Open))
            {
                return formatter.Deserialize(filesStream);
            }
        }

        public void Serialize(string filename)
        {
            var formatter = new BinaryFormatter();
            using (var fileStream = new FileStream(filename, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fileStream, this);
            }
        }
    }
}
