﻿using System.Collections.Generic;

namespace ControlSystem
{
    public interface IQuestion
    {
        /// <summary>
        /// Текст вопроса
        /// </summary>
        string Text { get; set; }
        /// <summary>
        /// Варианты ответа
        /// </summary>
        ICollection<IAnswer> Answers { get; set; }
    }
}
