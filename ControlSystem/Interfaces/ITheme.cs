﻿using System.Collections.Generic;

namespace ControlSystem
{
    public interface ITheme
    {
        /// <summary>
        /// Название темы
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Вопросы темы
        /// </summary>
        ICollection<IQuestion> Questions { get; set; }
        /// <summary>
        /// Время на прохождение теста в минутах
        /// </summary>
        int Time { get; set; }
    }
}
