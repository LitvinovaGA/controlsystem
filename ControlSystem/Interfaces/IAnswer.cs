﻿namespace ControlSystem
{
    public interface IAnswer
    {
        /// <summary>
        /// Текст ответа
        /// </summary>
        string Text { get; set; }
        /// <summary>
        /// Очки за ответ
        /// </summary>
        int Points { get; set; }
    }
}
