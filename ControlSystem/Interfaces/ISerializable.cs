﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlSystem.Interfaces
{
    public interface ISerializable
    {
        object Deserialize(string filename);
        void Serialize(string filename);
    }
}
