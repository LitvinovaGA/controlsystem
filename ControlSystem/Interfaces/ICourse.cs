﻿using System.Collections.Generic;

namespace ControlSystem
{
    public interface ICourse
    {
        /// <summary>
        /// Навзание курса
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Темы курса
        /// </summary>
        ICollection<ITheme> Themes { get; set; }
    }
}
