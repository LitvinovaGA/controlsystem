﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ControlSystem
{
    [Serializable]
    public class Answer : BaseSerializer, IAnswer, INotifyPropertyChanged
    {
        private string _text;
        private int _points;
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                if (value!=_text)
                {
                    _text = value;
                }
                OnPropertyChanged();
            }
        }
        public int Points
        {
            get
            {
                return _points;
            }
            set
            {
                if (_points!=value)
                {
                    _points = value;
                }
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
