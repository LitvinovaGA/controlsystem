﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ControlSystem
{
    [Serializable]
    public class Question : BaseSerializer, IQuestion, INotifyPropertyChanged
    {
        public Question()
        {
            Answers = new ObservableCollection<IAnswer>();
        }
        private string _text;
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                if (value!=_text)
                {
                    _text = value;
                    OnPropertyChanged();
                }
            }
        }
        public ICollection<IAnswer> Answers { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
