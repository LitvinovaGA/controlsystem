﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace ControlSystem
{
    [Serializable]
    public class Theme : BaseSerializer, ITheme, INotifyPropertyChanged
    {
        private string _name;
        private int _time;
        public Theme()
        {
            Questions = new ObservableCollection<IQuestion>();
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name!=value)
                {
                    _name = value;
                    OnPropertyChanged();
                }
            }
        }
        public int Time
        {
            get
            {
                return _time;
            }
            set
            {
                if (value!=_time)
                {
                    _time = value;
                    OnPropertyChanged();
                }
            }
         }
        public ICollection<IQuestion> Questions { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
